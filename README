----------------------------------------------------------------------
About xstr
----------------------------------------------------------------------

This package implements frequent string operations: searching, replacing,
splitting, matching. It is independent from the Str library, and can 
replace Str in many cases. Unlike Str, xstr is thread-safe. xstr does
not implement regular expressions in general, but an important subset.
Some operations of xstr are performed as quickly as by Str; if the string
to be processed is small, xstr is often faster than Str; if the string is
big, xstr is upto half as fast than Str. 

----------------------------------------------------------------------
BUILD:
----------------------------------------------------------------------

make all:
	Compiles the bytecode archive.
make opt:
	Compiles the native archive.


----------------------------------------------------------------------
INSTALL:
----------------------------------------------------------------------

The module can be installed using the installation method supported by the
"findlib" module. If you have installed "findlib", a "make install" copies
the compiled files to their standard places. The package name is then
"xstr". ("make uninstall" removes the package again.)

If you do not have "findlib", just copy the files manually where you like
them (cp *.cmi *.cma *.cmxa *.a <destination>).

After installation, a "make clean" removes the intermediate files.

----------------------------------------------------------------------
SPEED:
----------------------------------------------------------------------

In the "speed" directory some problems are solved using xstr and Str.
The results on a 486/133Mhz system:

				Using xstr		Using Str

Test Pattern matching 1: 	0.002400 seconds	0.001000 seconds
Test Pattern matching 2: 	0.000073 seconds	0.000229 seconds
Test Pattern matching 3: 	0.007460 seconds	0.004060 seconds
Test Pattern replacing 1: 	0.008500 seconds	0.004000 seconds
Test Pattern replacing 2: 	0.051300 seconds	0.042400 seconds
Test Splitting 1: 		0.020400 seconds	0.016100 seconds
Test Splitting 2: 		0.000220 seconds	0.000200 seconds
Test Substring searching: 	0.000890 seconds	0.000810 seconds
Test Unquoting 1: 		0.030200 seconds	0.031200 seconds
Test Unquoting 2: 		0.000195 seconds	0.000353 seconds

----------------------------------------------------------------------
CHANGES:
----------------------------------------------------------------------

Changed in version 0.2.1:
	Fix in mkset

Changed in version 0.2:
	Bugfix in replace_matched_substrings.

Changed in version 0.1.2:
	Updated URLs in documentation.

----------------------------------------------------------------------
AUTHOR:
----------------------------------------------------------------------

The module has been written by Gerd Stolpmann, 
gerd@gerd-stolpmann.de

You can download it from 
http://www.ocaml-programming.de/packages/.

This module has an entry in the O'Caml link database,
http://www.npc.de/ocaml/linkdb/


----------------------------------------------------------------------
LICENSE:
----------------------------------------------------------------------

See the LICENSE file.
