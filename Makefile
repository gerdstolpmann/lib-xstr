# make all: 		make bytecode archive
# make opt: 		make native archive
# make install: 	install bytecode archive, and if present, native archive
# make uninstall: 	uninstall package
# make clean: 		remove intermediate files
# make distclean: 	remove any superflous files
# make release: 	cleanup, create archive, tag CVS module 
#			(for developers)

#----------------------------------------------------------------------
# specific rules for this package:

OBJECTS  = xstr_split.cmo xstr_search.cmo xstr_match.cmo
XOBJECTS = xstr_split.cmx xstr_search.cmx xstr_match.cmx
ARCHIVE  = xstr.cma
XARCHIVE = xstr.cmxa
NAME     = xstr
#REQUIRES =
UNSAFE   =

# you may try this: (0% to 10% faster)
#UNSAFE   = -unsafe

all: $(ARCHIVE)

opt: $(XARCHIVE)

$(ARCHIVE): $(OBJECTS)
	$(OCAMLC) -a -o $(ARCHIVE) $(OBJECTS)

$(XARCHIVE): $(XOBJECTS)
	$(OCAMLOPT) -a -o $(XARCHIVE) $(XOBJECTS)

#----------------------------------------------------------------------
# general rules:

OPTIONS   =
OCAMLC    = ocamlc -g $(UNSAFE) $(OPTIONS) $(ROPTIONS)
OCAMLOPT  = ocamlopt $(UNSAFE) $(OPTIONS) $(ROPTIONS)
OCAMLDEP  = ocamldep $(OPTIONS)
OCAMLFIND = ocamlfind

depend: *.ml *.mli
	$(OCAMLDEP) *.ml *.mli >depend

#depend.pkg: Makefile
#	$(OCAMLFIND) use -p ROPTIONS= $(REQUIRES) >depend.pkg

.PHONY: install
install: all
	{ test ! -f $(XARCHIVE) || extra="*.cmxa *.a"; }; \
	$(OCAMLFIND) install $(NAME) *.mli *.cmi *.cma META $$extra

.PHONY: uninstall
uninstall:
	$(OCAMLFIND) remove $(NAME)

.PHONY: clean
clean:
	rm -f *.cmi *.cmo *.cma *.cmx *.o *.a *.cmxa
	$(MAKE) -C speed clean

.PHONY: distclean
distclean: clean
	rm -f *~ depend depend.pkg
	rm -f speed/*~

RELEASE: META
	awk '/version/ { print substr($$3,2,length($$3)-2) }' META >RELEASE

.PHONY: dist
dist: RELEASE
	r=`head -1 RELEASE`; cd ..; gtar czf $(NAME)-$$r.tar.gz --exclude='*/CVS*' --exclude="*/depend.pkg" --exclude="*/depend" $(NAME)

.PHONY: tag-release
tag-release: RELEASE
	r=`head -1 RELEASE | sed -e s/\\\./-/g`; cd ..; cvs tag -F $(NAME)-$$r $(NAME)

.PHONY: release
release: distclean
	$(MAKE) tag-release
	$(MAKE) dist

.ml.cmx:
	$(OCAMLOPT) -c $<

.ml.cmo:
	$(OCAMLC) -c $<

.mli.cmi:
	$(OCAMLC) -c $<

.SUFFIXES: .cmo .cmi .cmx .ml .mli

include depend
#include depend.pkg
