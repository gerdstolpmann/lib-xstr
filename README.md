# Xstr

Functions for string searching/matching/splitting

 - [Project page](http://projects.camlcity.org/projects/xstr.html)

 - [README with installation instructions](README)

 - [License](LICENSE)

